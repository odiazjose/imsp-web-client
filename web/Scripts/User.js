var User = function(){
    var self = this;
    var email = null;
    var pass = null;
    var name = null;
    var token = null;
    var contacts = [];
    var conversations = [];
    var target;
    var serverAddress = location.host;
    
    var autoUpdate = false;
    
    this.setEmail = function(mail){
        email = mail;
    };
    
    this.setPassword = function(password){
        pass = password;
    };
    
    this.setTarget = function(email) {
        target = email;
    };
    
    this.getEmail = function() {
        return email;
    };
    
    this.getUserName = function() {
        return name;
    };
    
    this.getContactList = function() {
        return contacts;
    };
    
    this.getTarget = function() {
        return target;
    };
    
    this.setConversations = function() {
        for(var i = 0; i < contacts.length; i++){
            var contactMail = contacts[i].email;
            if(conversations[contactMail] === undefined){
                console.log("Creating conversation for contact: " + contactMail);
                
                var conv = new Conversation(email, contactMail);
                conversations[contactMail] = conv;
            }
        }
    };
    
    this.getConversation = function() {
        return conversations[target];
    };
    
    this.login = function(callback) {
        if(email === null || pass === null) return;
        var xhr = new XHR();
        xhr.post(
            "http://" + serverAddress + "/IMSP_Server/webresources/Session/login",
            (function(data){
                if(data.result){
                    name = data.name;
                    token = data.token;
                    autoUpdate = true;
                }
                
                if(callback !== null) callback(data);
            }),
            {
                email: email,
                password: pass
            }
        );
    };
    
    this.logout = function(callback) {
        var xhr = new XHR();
        xhr.post(
            "http://" + serverAddress + "/IMSP_Server/webresources/Session/logout",
            (function(response){
                if(response.result){
                    email = null;
                    pass = null;
                    name = null;
                    token = null;
                    target = null;
                    contacts = [];
                    conversations = [];
                    
                    autoUpdate = false;
                    clearTimeout(contactUpdateRequest);
                    clearTimeout(messagesTimeout);
                }
                
                callback(response.msg);
            }),
            {
                token: token
            }
        );
    };
    
    this.addContact = function(contactEmail, callback) {
        if(contactEmail === null) return;
        var xhr = new XHR();
        xhr.post(
            "http://" + serverAddress + "/IMSP_Server/webresources/Contacts/add",
            callback,
            {
                token: token,
                target: contactEmail
            }
        );
    };
    
    var contactUpdateRequest;
    this.getContacts = function(callback) {
        var xhr = new XHR();
        xhr.get(
            "http://" + serverAddress + "/IMSP_Server/webresources/Contacts/get?token=" + token,
            (function(response){
                if(response.result){
                    contacts = JSON.parse(response.contacts);
                    self.setConversations();
                    if(autoUpdate) contactUpdateRequest = window.setTimeout((function(){
                        self.getContacts(null);
                    }), 1000 * 60 * 10);
                    if(callback !== null) callback(response);
                }
                else console.log("Contact list could not be updated. Error: " + response.msg);
            })
        );
    };
    
    this.sendMessage = function(message, media, timestamp, callback) {
        if(target === null || (message === null && media === null)) return;
        var xhr = new XHR();
        xhr.post(
            "http://" + serverAddress + "/IMSP_Server/webresources/Messages/send",
            callback, 
            {
                token: token,
                target: target,
                msg: message,
                media: media,
                timestamp: timestamp
            }
        );
    };
    
    var messagesTimeout = null;
    this.getMessages = function(update, callback) {
        var url = "http://" + serverAddress + "/IMSP_Server/webresources/Messages/get?";
        if(update) url += "mode=update&";
        url += "token=" + token + "&target=" + target;
        
        var xhr = new XHR();
        xhr.get(
            url,
            (function(response){
                if(response.result){
                    conversations[target].setMessages(JSON.parse(response.messages));
                    messagesTimeout = window.setTimeout((function(){
                        self.getMessages(true, null);
                    }), 3000);
                    if(callback !== null && callback !== undefined) callback(response);
                } else console.log("Messages could not be updated. Error: " + response.msg);
            })
        );
    };
    
    this.markMsgAsRead = function(messages, callback){
        var xhr = new XHR();
        xhr.post(
            "http://" + serverAddress + "/IMSP_Server/webresources/Messages/read", 
            (function(response){
                if(response && callback !== null)
                    callback(response);
        }),
        {
            messages: messages
        }
        );
    };
    
    this.getNotifications = function(callback) {
        var xhr = new XHR();
        xhr.get(
            "http://" + serverAddress + "/IMSP_Server/webresources/Messages/notify?token=" + token, 
            (function(response){
                if(response.result){
                    callback(response);
                }
            })
        );
    };
};