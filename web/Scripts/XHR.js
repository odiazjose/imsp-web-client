XHR = function(){
    var jsonResponse = true;
    var asynchronousMode = true;
    var debugMode = false;
    var xhr = new XMLHttpRequest();

    this.get = function(url, callback){
        xhr.open("GET", url, asynchronousMode);

        xhr.onreadystatechange = function(){
                if(xhr.status === 200 && xhr.readyState === 4){
                    if(debugMode) console.log("GET plain text response: " + xhr.responseText);
                    if(jsonResponse){
                        var json = JSON.parse(xhr.responseText);
                        callback(json);
                        if(debugMode) console.log("GET JSON response: " + json);
                    } else callback(xhr.responseText);
                }
        };

        xhr.send();
    };

    this.post = function(url, callback, json){
        var parameters = "";
        var firstParameter = true;

        for(var property in json){
            if(firstParameter === false) parameters += "&";
            else firstParameter = false;

            parameters += property + "=" + this.getPropertyValue(json, property);
        }
        
        xhr.open("POST", url, asynchronousMode);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

        xhr.onreadystatechange = function(){
            if(xhr.status === 200 && xhr.readyState === 4){
                if(debugMode) console.log("POST plain text response: " + xhr.responseText);
                if(jsonResponse){
                        var json = JSON.parse(xhr.responseText);
                        callback(json);
                        if(debugMode) console.log("GET JSON response: " + json);
                } else callback(xhr.responseText);
            }
        };
        if(debugMode) console.log("POST params: " + parameters);
        xhr.send(parameters);
    };

    this.getPropertyValue = function(object, property){
        var propertyType = typeof(object[property]);

        if(propertyType === "string" || propertyType === "number" || propertyType === "boolean"){
            return object[property];
        }else if(Array.isArray(object[property])){
            return JSON.stringify(object[property]);
        }else if(propertyType === "object"){
            return JSON.stringify(object[property]);
        }else{
            return null;
        }
    };
    
    this.responseAsJSON = function(flag){
        jsonResponse = flag;
    };
    
    this.setAsynchronousMode = function(flag) {
        asynchronousMode = flag;
    };
    
    this.debug = function(flag) {
        debugMode = flag;
    };
};