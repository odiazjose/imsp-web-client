var Conversation = function(userEmail, targetEmail) {
    var init = false;
    var user = userEmail;
    var target = targetEmail;
    var msgID = [];
    var messages_nmd = [];
    var messages_new = [];
    
    var recordMessage = function(id) {
        msgID.push(id);
    };
    
    var isRecorded = function(id) {
        var found = false;
        
        for(var recID in msgID){
            if(msgID[recID] === id){
                found = true;
                break;
            }
        }
        
        return found;
    };
    
    this.setMessages = function(data) {
        for(var msg = 0; msg < data.length; msg++) {
            if(!isRecorded(data[msg].id)) {
                recordMessage(data[msg].id);
                messages_nmd[data[msg].id] = data[msg];
                if(init) messages_new[data[msg].id] = data[msg];
            }
        }
        
        if(init === false) init = true;
    };
    
    this.getMessages = function(){
        var messages = "[";
        
        for(var index in messages_nmd){
            messages += JSON.stringify(messages_nmd[index]);
            if(index < messages_nmd.length - 1) messages += ",";
        }
        messages += "]";
        return JSON.parse(messages);
    };
    
    this.getNewMessages = function() {
        var messages = "[";
        
        for(var index in messages_new){
            messages += JSON.stringify(messages_new[index]);
            if(index < messages_new.length - 1) messages += ",";
        }
        messages += "]";
        messages_new = [];
        return JSON.parse(messages);
    };
    
    this.isEmpty = function() {
        return (messages_nmd.length === 0);
    };
};